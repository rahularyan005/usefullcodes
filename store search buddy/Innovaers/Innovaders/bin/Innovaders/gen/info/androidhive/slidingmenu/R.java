/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package info.androidhive.slidingmenu;

public final class R {
    public static final class array {
        public static final int circles=0x7f080002;
        /**  Nav Drawer List Item Icons 
 Keep them in order as the titles are in 
         */
        public static final int nav_drawer_icons=0x7f080001;
        /**  Nav Drawer Menu Items 
         */
        public static final int nav_drawer_items=0x7f080000;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int back=0x7f05000f;
        public static final int back1=0x7f050010;
        public static final int counter_text_bg=0x7f050004;
        public static final int counter_text_color=0x7f050005;
        public static final int list_background=0x7f050001;
        public static final int list_background_pressed=0x7f050002;
        public static final int list_divider=0x7f050003;
        public static final int list_item_title=0x7f050000;
        public static final int text=0x7f05000e;
        public static final int theme1=0x7f050006;
        public static final int theme2=0x7f050007;
        public static final int theme3=0x7f050008;
        public static final int theme4=0x7f050009;
        public static final int theme5=0x7f05000a;
        public static final int theme6=0x7f05000b;
        public static final int theme7=0x7f05000c;
        public static final int theme8=0x7f05000d;
        public static final int white_txt=0x7f050011;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int about=0x7f020000;
        public static final int add=0x7f020001;
        public static final int app_icon=0x7f020002;
        public static final int circle1=0x7f020003;
        public static final int circle2=0x7f020004;
        public static final int circle3=0x7f020005;
        public static final int circle4=0x7f020006;
        public static final int circle5=0x7f020007;
        public static final int circle6=0x7f020008;
        public static final int circle7=0x7f020009;
        public static final int circle8=0x7f02000a;
        public static final int circle_box=0x7f02000b;
        public static final int counter_bg=0x7f02000c;
        public static final int ic_communities=0x7f02000d;
        public static final int ic_drawer=0x7f02000e;
        public static final int ic_home=0x7f02000f;
        public static final int ic_launcher=0x7f020010;
        public static final int ic_pages=0x7f020011;
        public static final int ic_people=0x7f020012;
        public static final int ic_photos=0x7f020013;
        public static final int ic_whats_hot=0x7f020014;
        public static final int list_item_bg_normal=0x7f020015;
        public static final int list_item_bg_pressed=0x7f020016;
        public static final int list_item_box=0x7f020017;
        public static final int list_selector=0x7f020018;
        public static final int map=0x7f020019;
        public static final int search=0x7f02001a;
        public static final int share=0x7f02001b;
        public static final int transparent=0x7f02001c;
    }
    public static final class id {
        public static final int LinearLayout1=0x7f0b0000;
        public static final int aboutus=0x7f0b002e;
        public static final int action_check_updates=0x7f0b0006;
        public static final int action_help=0x7f0b0005;
        public static final int action_location_found=0x7f0b0003;
        public static final int action_refresh=0x7f0b0004;
        public static final int action_settings=0x7f0b002a;
        public static final int add=0x7f0b002b;
        public static final int address=0x7f0b0008;
        public static final int assign=0x7f0b000d;
        public static final int bhu=0x7f0b0027;
        public static final int button1=0x7f0b0029;
        public static final int cantt=0x7f0b0024;
        public static final int circle=0x7f0b0028;
        public static final int city=0x7f0b000a;
        public static final int complex=0x7f0b0012;
        public static final int contact=0x7f0b000e;
        public static final int counter=0x7f0b001b;
        public static final int dlw=0x7f0b0023;
        public static final int drawer_layout=0x7f0b0016;
        public static final int email=0x7f0b000f;
        public static final int frame_container=0x7f0b0017;
        public static final int icon=0x7f0b0019;
        public static final int lahartara=0x7f0b0021;
        public static final int latitude=0x7f0b000b;
        public static final int list=0x7f0b0015;
        public static final int list_slidermenu=0x7f0b0018;
        public static final int longitude=0x7f0b000c;
        public static final int mahmoorganj=0x7f0b0022;
        public static final int name=0x7f0b0007;
        public static final int pandeypur=0x7f0b0020;
        public static final int rathyatra=0x7f0b0026;
        public static final int reset=0x7f0b0013;
        public static final int search=0x7f0b002c;
        public static final int searchbar=0x7f0b001c;
        public static final int section=0x7f0b001f;
        public static final int share=0x7f0b002d;
        public static final int sigra=0x7f0b0025;
        public static final int spincircle=0x7f0b0009;
        public static final int submit=0x7f0b0014;
        public static final int textView1=0x7f0b0001;
        public static final int textView2=0x7f0b0002;
        public static final int textView3=0x7f0b0011;
        public static final int title=0x7f0b001a;
        public static final int website=0x7f0b0010;
        public static final int zip=0x7f0b001d;
        public static final int zipbutton=0x7f0b001e;
    }
    public static final class layout {
        public static final int about_us=0x7f030000;
        public static final int actionbar=0x7f030001;
        public static final int activity_add=0x7f030002;
        public static final int activity_list=0x7f030003;
        public static final int activity_main=0x7f030004;
        public static final int drawer_list_item=0x7f030005;
        public static final int fragment_home=0x7f030006;
        public static final int searchable=0x7f030007;
        public static final int store_item=0x7f030008;
    }
    public static final class menu {
        public static final int add=0x7f0a0000;
        public static final int list=0x7f0a0001;
        public static final int main=0x7f0a0002;
        public static final int voice=0x7f0a0003;
    }
    public static final class string {
        public static final int action_settings=0x7f070001;
        public static final int app_name=0x7f070000;
        /**  Content Description 
         */
        public static final int desc_list_item_icon=0x7f070005;
        public static final int drawer_close=0x7f070004;
        public static final int drawer_open=0x7f070003;
        public static final int hello_world=0x7f070002;
        public static final int prompt=0x7f070009;
        public static final int title_activity_add=0x7f070006;
        public static final int title_activity_list=0x7f070007;
        public static final int title_activity_voice=0x7f070008;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f090000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f090001;
    }
    public static final class xml {
        public static final int fragment_whats_hot=0x7f040000;
    }
}
