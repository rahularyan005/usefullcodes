package hackathon.innovaders;





import info.androidhive.slidingmenu.R;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Add extends Activity {
	
	
	
	String name,address,circle,city,contact,email,website,complex,latitude,longitude;
	String task,id;
	private DataBaseHelp mHelper;
	private SQLiteDatabase dataBase;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);
		final EditText edname=(EditText)findViewById(R.id.name);
		final EditText edaddress=(EditText)findViewById(R.id.address);
		final EditText edcity=(EditText)findViewById(R.id.city);
		final EditText edcontact=(EditText)findViewById(R.id.contact);
		final EditText edemail=(EditText)findViewById(R.id.email);
		final EditText edwebsite=(EditText)findViewById(R.id.website);
		final EditText edcomplex=(EditText)findViewById(R.id.complex);
		final EditText edlatitude=(EditText)findViewById(R.id.latitude);
		final EditText edlongitude=(EditText)findViewById(R.id.longitude);
		Button assign=(Button)findViewById(R.id.assign);
		Button reset=(Button)findViewById(R.id.reset);
		Button submit=(Button)findViewById(R.id.submit);
       final Spinner circleselect=(Spinner) findViewById(R.id.spincircle);
       
       //finding whether to update or add by extas sent
       task=getIntent().getExtras().getString("task");
       if(task.contains("update")){
    	   
    	   String name=getIntent().getExtras().getString("name");
    	   String address=getIntent().getExtras().getString("address");
    	   String city=getIntent().getExtras().getString("city");
    	   String contact=getIntent().getExtras().getString("contact");
    	   String email=getIntent().getExtras().getString("email");
    	   String website=getIntent().getExtras().getString("website");
    	   String complex=getIntent().getExtras().getString("complex");
    	   String latitude=getIntent().getExtras().getString("latitude");
    	   String longitude=getIntent().getExtras().getString("longitude");
    	   
    	   edname.setText(name);
			edaddress.setText(address);
			edcity.setText(city);
			edcontact.setText(contact);
			edemail.setText(email);
			edwebsite.setText(website);
			edcomplex.setText(complex);
			edlatitude.setText(latitude);
			edlongitude.setText(longitude);
			id=getIntent().getExtras().getString("id");
			submit.setText("Update");

    
       }
		circleselect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view,
					int i, long l) {
				// TODO Auto-generated method stub
				String[] spincircle=getResources().getStringArray(R.array.circles);
				circle=spincircle[i];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//Action Bar
		ActionBar actionBar = getActionBar();
		 
        // Enabling Up / Back navigation
        actionBar.setDisplayHomeAsUpEnabled(true);
        //finding latitudes and longitudes
		assign.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				GPST gps;
				 gps = new GPST(Add.this);
				 
	                // check if GPS enabled     
	                if(gps.canGetLocation()){
	                     
	                    double alatitude = gps.getLatitude();
	                    double alongitude = gps.getLongitude();
	                    
	                    edlatitude.setText(Double.toString(alatitude));
	                    edlongitude.setText(Double.toString(alongitude));
	                     
	                    // \n is for new line
	                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + alatitude + "\nLong: " + alongitude, Toast.LENGTH_SHORT).show();    
	                }else{
	                    // can't get location
	                    // GPS or Network is not enabled
	                    // Ask user to enable GPS/network in settings
	                    gps.showSettingsAlert();
	                }
				
			}
		});
		reset.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				edname.setText("");
				edaddress.setText("");
				edcity.setText("");
				edcontact.setText("");
				edemail.setText("");
				edwebsite.setText("");
				edcomplex.setText("");
				edlatitude.setText("");
				edlongitude.setText("");
			}
		});
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				name=edname.getText().toString();
				address=edaddress.getText().toString();
				
				city=edcity.getText().toString();
				if(city.contentEquals(""))city="Unavailable";
				contact=edcontact.getText().toString();
				if(contact.contentEquals(""))contact="Unavailable";
				email=edemail.getText().toString();
				if(email.contentEquals(""))email="Unavailable";
				website=edwebsite.getText().toString();
				if(website.contentEquals(""))website="Unavailable";
				complex=edcomplex.getText().toString();
				if(complex.contentEquals(""))complex="Unavailable";
				
				
				latitude=edlatitude.getText().toString();
				if(latitude.contentEquals(""))latitude="0";
				longitude=edlongitude.getText().toString();
				if(longitude.contentEquals(""))longitude="0";
				
				mHelper=new DataBaseHelp(getApplicationContext());
				if(name.length()>0 && address.length()>0)
		        {
					 mHelper= new DataBaseHelp(getApplicationContext());
					saveData();
		        }
		        else
		        {
		            AlertDialog.Builder alertBuilder=new AlertDialog.Builder(Add.this);
		            alertBuilder.setTitle("Warning!!!");
		            alertBuilder.setMessage("Shop Name and Address can't be empty!");
		            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		                
		                public void onClick(DialogInterface dialog, int which) {
		                       dialog.cancel();
		                    
		                }
		            });
		            alertBuilder.create().show();
		        }
				
				
				
				
				
				
			}
		});
		
		
	}
	 private void saveData(){
		
		dataBase=mHelper.getWritableDatabase();
			ContentValues values=new ContentValues();
			
			values.put(DataBaseHelp.KEY_NAME, name);
			values.put(DataBaseHelp.KEY_ADDRESS, address);
			values.put(DataBaseHelp.KEY_LAT, latitude);
			values.put(DataBaseHelp.KEY_LONG, longitude);
			values.put(DataBaseHelp.KEY_CIRCLE, circle );
			values.put(DataBaseHelp.KEY_CITY, city);
			values.put(DataBaseHelp.KEY_CONT, contact);
			values.put(DataBaseHelp.KEY_EMAIL, email);
			values.put(DataBaseHelp.KEY_WEB, website);
			values.put(DataBaseHelp.KEY_COMP, complex);
	        
	        
	        //if(isUpdate)
	        //{    
	            //update database with new data 
	        //    dataBase.update(DbHelper.TABLE_NAME, values, DbHelper.KEY_ID+"="+id, null);
	        //}
	        //else
	        //{
	            //insert data into database
			if(task.contains("add")){
	           dataBase.insert(DataBaseHelp.TABLE_NAME, null, values);
			}
			if(task.contains("update")){
			   dataBase.update(DataBaseHelp.TABLE_NAME, values, DataBaseHelp.KEY_ID+"= '"+id+"'", null);
			}
	        //}
	        //close database
	       dataBase.close();
	        Toast.makeText(getApplicationContext(), "Store Successfully Added!", Toast.LENGTH_SHORT).show();
	        finish();
	        
	        
	    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add, menu);
		return true;
	}

}
