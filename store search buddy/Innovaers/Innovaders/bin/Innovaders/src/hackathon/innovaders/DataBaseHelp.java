package hackathon.innovaders;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelp extends SQLiteOpenHelper {
	
	static String DATABASE_NAME="mainDatabase.db";
	public static final String TABLE_NAME="store";
	public static final String KEY_ID="id";
	public static final String KEY_NAME="name";
	public static final String KEY_ADDRESS="address";
	public static final String KEY_LAT="latitude";
	public static final String KEY_LONG="longitude";
	public static final String KEY_CIRCLE="circle";
	public static final String KEY_CITY="city";
	public static final String KEY_CONT="contact";
	public static final String KEY_EMAIL="email";
	public static final String KEY_WEB="web";
	public static final String KEY_COMP="complex";
	
	public DataBaseHelp(Context context){
		super(context,DATABASE_NAME,null,11);	
}	

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+" ("+KEY_ID+" INTEGER PRIMARY KEY, "+KEY_NAME+" TEXT, "+KEY_ADDRESS+" TEXT, "+KEY_CIRCLE+" TEXT, "+KEY_CITY+" TEXT, "+KEY_CONT+" TEXT, "+KEY_EMAIL+" TEXT, "+KEY_WEB+" TEXT, "+KEY_COMP+" TEXT, "+KEY_LAT+" TEXT, "+KEY_LONG+" TEXT)";
        db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
		
		
	}

}
