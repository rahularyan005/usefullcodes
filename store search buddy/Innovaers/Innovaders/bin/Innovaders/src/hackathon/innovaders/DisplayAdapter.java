package hackathon.innovaders;

import info.androidhive.slidingmenu.R;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DisplayAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<String> aname;
	private ArrayList<String> aaddress;
	private ArrayList<String> acircle;
	private ArrayList<String> acity;
	private ArrayList<String> acontact;
	private ArrayList<String> aemail;
	private ArrayList<String> awebsite;
	private ArrayList<String> acomplex;
	
	private ArrayList<StoreRecord> stores= new ArrayList<StoreRecord>();
	
	public DisplayAdapter(Context context, ArrayList<String> aname,ArrayList<String> aaddress, ArrayList<String> acircle,ArrayList<String> acity, ArrayList<String> acontact, ArrayList<String> aemail,ArrayList<String> awebsite, ArrayList<String> acomplex,ArrayList<String> alatitude,ArrayList<String>alongitude){
		for(int i=0;i<=aname.size()-1;i++){
		stores.add(new StoreRecord( aname.get(i), aaddress.get(i),acircle.get(i),acity.get(i),acontact.get(i), aemail.get(i), awebsite.get(i), acomplex.get(i), alatitude.get(i),alongitude.get(i)));
		}
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return stores.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return getItem(position);
	}

	@Override
	public long getItemId(int index) {
		// TODO Auto-generated method stub
		return index;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater;
		if(view==null){
			inflater=LayoutInflater.from(parent.getContext());
			view=inflater.inflate(R.layout.store_item,parent,false);
		}
		StoreRecord store=stores.get(index);
		
		TextView name=(TextView)view.findViewById(R.id.name);
	
		name.setText(store.getName().toUpperCase());
		TextView address=(TextView)view.findViewById(R.id.address);
		address.setText(store.getAddress()+"  "+store.getCity());
		TextView contact=(TextView)view.findViewById(R.id.contact);
		contact.setText(store.getContact()+" : "+store.getEmail());
		TextView circle=(TextView)view.findViewById(R.id.circle);
		if(store.getCircle().contains("Pandeypur")){
			view.setBackgroundResource(R.color.theme1);
			circle.setText("P");
		}
		if(store.getCircle().contains("Lahartara")){
			view.setBackgroundResource(R.drawable.list_item_box);
			circle.setText("L");
		}
		if(store.getCircle().contains("Mahmoorganj")){
			view.setBackgroundResource(R.color.theme3);
			circle.setText("M");
		}
		if(store.getCircle().contains("DLW")){
			view.setBackgroundResource(R.color.theme4);
			circle.setText("D");
		}
		if(store.getCircle().contains("Cantt")){
			view.setBackgroundResource(R.color.theme5);
			circle.setText("C");
		}
		if(store.getCircle().contains("Sigra")){
			view.setBackgroundResource(R.color.theme6);
			circle.setText("S");
		}
		if(store.getCircle().contains("Rathyatra")){
			view.setBackgroundResource(R.color.theme7);
			circle.setText("R");
		}
		if(store.getCircle().contains("BHU")){
			view.setBackgroundResource(R.color.theme8);
			circle.setText("B");
		}
		
		return view;
	}

}
