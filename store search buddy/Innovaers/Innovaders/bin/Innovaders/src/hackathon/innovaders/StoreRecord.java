package hackathon.innovaders;

public class StoreRecord {
	
	public String name;
	public String address;
	public String circle;
	public String city;
	public String contact;
	public String email;
	public String website;
	public String complex;
	public String latitude;
	public String longitude;
	
	public StoreRecord(String name,String address,String circle,String city,String contact,String email,String website,String complex,String latitude,String longitude){
		this.name=name;
		this.address=address;
		this.circle=circle;
		this.city=city;
		this.contact=contact;
		this.email=email;
		this.website=website;
		this.complex=complex;
		this.latitude=latitude;
		this.longitude=longitude;
	}
	
	public String getName(){return name;}
	public String getAddress(){return address;}
	public String getCircle(){return circle;}
	public String getCity(){return city;}
	public String getContact(){return contact;}
	public String getEmail(){return email;}
	public String getWebsite(){return website;}
	public String getComplex(){return complex;}
	public String getLatitude(){return latitude;}
	public String getLongitude(){return longitude;}
	
	public void setName(String name){this.name=name;}
	public void setAddress(String address){this.address=address;}
	public void setCircle(String circle){this.circle=circle;}
	public void setCity(String city){this.city=city;}
	public void setContact(String contact){this.contact=contact;}
	public void setEmail(String email){this.email=email;}
	public void setWebsite(String website){this.website=website;}
	public void setComplex(String complex){this.complex=complex;}
	public void setLatitude(String latitude){this.latitude=latitude;}
	public void setLongitude(String longitude){this.longitude=longitude;}
	

}
