package hackathon.innovaders;

import info.androidhive.slidingmenu.R;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class HomeFragment extends Fragment {
	
	public HomeFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View V = inflater.inflate(R.layout.fragment_home, container, false);
        LinearLayout pandeypur=(LinearLayout)V.findViewById(R.id.pandeypur);
        LinearLayout lahartara=(LinearLayout)V.findViewById(R.id.lahartara);
        LinearLayout mahmoorganj=(LinearLayout)V.findViewById(R.id.mahmoorganj);
        LinearLayout dlw=(LinearLayout)V.findViewById(R.id.dlw);
        LinearLayout cantt=(LinearLayout)V.findViewById(R.id.cantt);
        LinearLayout sigra=(LinearLayout)V.findViewById(R.id.sigra);
        LinearLayout rathyatra=(LinearLayout)V.findViewById(R.id.rathyatra);
        LinearLayout bhu=(LinearLayout)V.findViewById(R.id.bhu);
        ImageButton zipsearch=(ImageButton)V.findViewById(R.id.zipbutton);
        final AutoCompleteTextView zip=(AutoCompleteTextView)V.findViewById(R.id.zip);
        zipsearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String circle="";
				String error="Sorry, this PIN Code not exits";
				circle=zip.getText().toString();
				int Pin=Integer.parseInt(circle);
				switch(Pin){
				case 221010:circle="Pandeypur";
				           break;
				case 221006:circle="Lahartara";
							break;
				case 221003:circle="Mahmoorganj";
							break;
				case 221004:circle="DLW";
							break;
				case 221002:circle="Cantt";
							break;
				case 221001:circle="Sigra";
							break;
				case 221007:circle="Rathyatra";
							break;
				case 221005:circle="BHU";
							break;
				default:circle="null";
				
				
				
				}
				
				if(!circle.contains("null")){
				Intent callcircle=new Intent(getActivity(),List.class);
				callcircle.putExtra("query", circle);
				if(circle.contains(""));
				startActivityForResult(callcircle,1);
				}
				else{
					Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
				}
			}
		});
        pandeypur.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent pandeypur=new Intent(getActivity(),List.class);
				pandeypur.putExtra("query", "Pandeypur");
				startActivityForResult(pandeypur,1);
			}
		});
        lahartara.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent lahartara=new Intent(getActivity(),List.class);
				lahartara.putExtra("query", "Lahartara");
				startActivityForResult(lahartara,2);
			}
		});
        mahmoorganj.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent mahmoorganj=new Intent(getActivity(),List.class);
				mahmoorganj.putExtra("query", "Mahmoorganj");
				startActivityForResult(mahmoorganj,3);
			}
		});
dlw.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Intent dlw=new Intent(getActivity(),List.class);
			dlw.putExtra("query", "DLW");
		startActivityForResult(dlw,4);
	}
});
        cantt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent cantt=new Intent(getActivity(),List.class);
				cantt.putExtra("query", "Cantt");
				startActivityForResult(cantt,5);
			}
		});
        sigra.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent sigra=new Intent(getActivity(),List.class);
				sigra.putExtra("query", "Sigra");
				startActivityForResult(sigra,6);
			}
		});
        rathyatra.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent rathyatra=new Intent(getActivity(),List.class);
				rathyatra.putExtra("query", "Rathyatra");
				startActivityForResult(rathyatra,7);
			}
		});
        bhu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent bhu=new Intent(getActivity(),List.class);
				bhu.putExtra("query", "BHU");
				startActivityForResult(bhu,8);	
			}
		});
        
        
        return V;
        
      
        
        
        
        
        
        
    }
}
