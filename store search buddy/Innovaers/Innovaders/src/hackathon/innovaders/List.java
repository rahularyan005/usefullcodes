package hackathon.innovaders;

import info.androidhive.slidingmenu.R;

import java.util.ArrayList;

import android.net.Uri;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class List extends Activity {
	 
	
	
	 private DataBaseHelp mHelper;
	    private SQLiteDatabase dataBase;
	    private AlertDialog.Builder build;
	    private ListView storeList;
	    private DisplayAdapter disp;
	    private ArrayList<String> aname = new ArrayList<String>();
	    private ArrayList<String> aaddress = new ArrayList<String>();
	   
	    private ArrayList<String> acity = new ArrayList<String>();
	    private ArrayList<String> acircle= new ArrayList<String>();
	    private ArrayList<String> acontact = new ArrayList<String>();
	    private ArrayList<String> aemail = new ArrayList<String>();
	    private ArrayList<String> awebsite = new ArrayList<String>();
	    private ArrayList<String> acomplex = new ArrayList<String>();
	    
	    private ArrayList<String> alatitude = new ArrayList<String>();
	    private ArrayList<String> alongitude= new ArrayList<String>();
	    
	    private ArrayList<StoreRecord> stores= new ArrayList<StoreRecord>();
	    String quest;
	    Cursor mCursor;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		//Action Bar
				ActionBar actionBar = getActionBar();
				 
		        // Enabling Up / Back navigation
		        actionBar.setDisplayHomeAsUpEnabled(true);
		        
		        
		        //finding latitudes and longitudes
		 quest=getIntent().getExtras().getString("query");
		
		
		 storeList=(ListView)findViewById(R.id.list);
	     mHelper=new DataBaseHelp(getApplicationContext());
	     //intially fill up data in oncreate method
	       fillData(quest);
	 
			//to fill the array of object to use it in following
			for(int i=0;i<=aname.size()-1;i++){
				stores.add(new StoreRecord( aname.get(i), aaddress.get(i),acircle.get(i),acity.get(i),acontact.get(i), aemail.get(i), awebsite.get(i), acomplex.get(i), alatitude.get(i),alongitude.get(i)));
				}
			
			storeList.setClickable(true);
			storeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int index,
					long arg3) {
				// TODO Auto-generated method stub
				
				String lati=stores.get(index).getLatitude();
				String longi=stores.get(index).getLongitude();
				Double latitude=Double.parseDouble(lati);
				Double longitude=Double.parseDouble(longi);
				String label="Searching";
				String uriBegin = "geo:" + latitude + "," + longitude;
				String query = latitude + "," + longitude + "(" + label + ")";
				String encodedQuery = Uri.encode(query);
				String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
				Uri uri = Uri.parse(uriString);
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
				startActivity(intent);
				
			}
			});
			storeList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View arg1,final int index, long arg3) {
					// TODO Auto-generated method stub
					
					build = new AlertDialog.Builder(List.this);
	                build.setTitle(stores.get(index).getName() + " " + stores.get(index).getAddress());
	                build.setMessage("You want to..");
	                
	                build.setPositiveButton("Delete this",new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            int ind=index;
                            Toast.makeText( getApplicationContext(),
                                   stores.get(index).getName() + " "
                                            + stores.get(index).getAddress()
                                            + " is deleted.", 3000).show();

                            dataBase.delete(DataBaseHelp.TABLE_NAME,DataBaseHelp.KEY_NAME + "= '"+stores.get(ind).getName()+"'" , null);
                            
                            updateList();
                          
                        }
                    });
	                build.setNegativeButton("Update this", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                        	Intent update=new Intent(List.this,Add.class);
                        	update.putExtra("id", index);
                        	update.putExtra("address", stores.get(index).getAddress());
                        	update.putExtra("city", stores.get(index).getCity());
                        	update.putExtra("contact", stores.get(index).getContact());
                        	update.putExtra("email", stores.get(index).getEmail());
                        	update.putExtra("website", stores.get(index).getWebsite());
                        	update.putExtra("complex", stores.get(index).getComplex());
                        	update.putExtra("latitude", stores.get(index).getLatitude());
                        	update.putExtra("longitude", stores.get(index).getLongitude());
                        	update.putExtra("task","update");
                        	startActivityForResult(update,3);
                                dialog.cancel();
                        }
                    });
	                AlertDialog alert = build.create();
	                alert.show();
	                	
	                
	                	
	                	
	                	
	                
	                return true;	
	                	
	                }});
	                

	              
					
				}
				
private void fillData(String query){
	 dataBase=mHelper.getWritableDatabase();
	 if(query.contains("global")){
		 mCursor=dataBase.rawQuery("SELECT * FROM "+DataBaseHelp.TABLE_NAME, null);
	 }else{
		mCursor=dataBase.rawQuery("SELECT * FROM "+DataBaseHelp.TABLE_NAME+" WHERE ("+DataBaseHelp.KEY_CIRCLE+" ='"+query+"')", null);
	   }
		if(mCursor.moveToFirst()){
			do{
			aname.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_NAME)));
			aaddress.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_ADDRESS)));
			acircle.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_CIRCLE)));
			acity.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_CITY)));
			acontact.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_CONT)));
			aemail.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_EMAIL)));
			awebsite.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_WEB)));
			acomplex.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_COMP)));
			alatitude.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_LAT)));
			alongitude.add(mCursor.getString(mCursor.getColumnIndex(DataBaseHelp.KEY_LONG)));
			}while(mCursor.moveToNext());
		}	
			
		//txt.setText(String.valueOf(aname.size()));
		//for(int i=0;i<=aname.size()-1;i++){
			//txt.append(aname.get(i)+" "+aaddress.get(i)+" "+" "+acity.get(i)+" "+acontact.get(i)+" "+aemail.get(i)+" "+awebsite.get(i)+" "+acomplex.get(i)+"\n");   //prints all strings in the array
		//}
		disp= new DisplayAdapter(getApplicationContext(), aname, aaddress, acircle, acity, acontact, aemail, awebsite, acomplex, alatitude,alongitude);
		storeList.setAdapter(disp);
		mCursor.close();	
}	
			  
			
public void updateList(){

    runOnUiThread(new Runnable() {

        public void run() {
            // TODO Auto-generated method stub
            disp.notifyDataSetChanged(); 
            storeList.refreshDrawableState(); 
            storeList.invalidate(); 
            Toast.makeText(List.this, "AHHHH", Toast.LENGTH_SHORT).show();
        }
    });
}

	
	
	
	


	



	








	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list, menu);
		return true;
	}
	

}
