package hackathon.innovaders;

import android.content.Context;
import android.util.AttributeSet;

import android.widget.LinearLayout;

public class Rectangle23 extends LinearLayout {
	 public Rectangle23(Context context) {
	        super(context);
	    }

	    public Rectangle23(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	    }

	    public Rectangle23(Context context, AttributeSet attrs) {
	        super(context, attrs);
	    }


	    @Override public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    	super.onMeasure(widthMeasureSpec, widthMeasureSpec);
	        int width = MeasureSpec.getSize(widthMeasureSpec);
	        
	        //height=width*(2/3);
	        setMeasuredDimension(width, (width/3)*2);
	    }

}
