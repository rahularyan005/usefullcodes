package hackathon.innovaders;

import android.content.Context;
import android.util.AttributeSet;

import android.widget.LinearLayout;

public class Square13 extends LinearLayout {
	
	public Square13(Context context) {
        super(context);
    }

    public Square13(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public Square13(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        
        
        setMeasuredDimension(height/3, height/3);
    }

}
