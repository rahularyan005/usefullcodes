package hackathon.innovaders;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;

import android.widget.LinearLayout;

public class Square23 extends LinearLayout {
	
	public Square23(Context context) {
        super(context);
    }

    public Square23(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public Square23(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        
        
        setMeasuredDimension((height*2)/3, (height*2)/3);
    }

}
