package hackathon.innovaders;

import android.content.Context;
import android.util.AttributeSet;

import android.widget.LinearLayout;

public class Square31 extends LinearLayout {
	public Square31(Context context) {
        super(context);
    }

    public Square31(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public Square31(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        
        
        setMeasuredDimension(width/3, width/3);
    }

}
