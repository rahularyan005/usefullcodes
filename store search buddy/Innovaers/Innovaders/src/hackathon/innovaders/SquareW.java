package hackathon.innovaders;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class SquareW extends LinearLayout{

    public SquareW(Context context) {
        super(context);
    }

    public SquareW(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public SquareW(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        
        
        setMeasuredDimension(width, width);
    }
    
}
